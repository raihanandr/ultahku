from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, 'home.html')

def foto(request):
    return render(request, 'foto.html')

def asli(request):
    return render(request, 'fotoAsli.html')

def beneran(request):
    return render(request, 'fotoBeneran.html')