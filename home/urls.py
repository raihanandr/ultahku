from django.urls import path

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('foto', views.foto, name='foto'),
    path('asli', views.asli, name='asli'),
    path('beneran', views.beneran, name='beneran'),
]
